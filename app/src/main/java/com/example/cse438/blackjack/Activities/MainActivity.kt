package com.example.cse438.blackjack.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.cse438.blackjack.Player
import com.example.cse438.blackjack.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        register_button.setOnClickListener {
            performRegister()
        }

        already_have_account_textview.setOnClickListener{
            Log.d("MainActivity", "Show login activity")

            //launch login activity
            val intent= Intent(this, LoginActivity::class.java)
            startActivity(intent)

        }
    }

        private fun performRegister() {
            val email = email_register.text.toString()
            val password = password_register.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Please enter text in email/pw", Toast.LENGTH_SHORT).show()
                return
            }
//
//            Log.d("MainActivity", "Email is: " + email)
//            Log.d("MainActivity", "Password: $password")

            // Firebase Authentication to create a user with email and password
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (!it.isSuccessful) return@addOnCompleteListener

                    // else if successful
                    Log.d("Main", "Successfully created user with uid: ${it.result!!.user.uid}")
                    saveUserToFirebaseDatabase()

                    val intent = Intent(this, GameActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)

                }
                .addOnFailureListener{
                    Log.d("Main", "Failed to create user: ${it.message}")
                    Toast.makeText(this, "Failed to create user: ${it.message}", Toast.LENGTH_SHORT).show()
                }

        }

    private fun saveUserToFirebaseDatabase(){
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = Player(uid, email_register.text.toString(), 0, 0)

        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "We saved our user to the Firebase database")
            }
            .addOnFailureListener {
                Log.d("RegisterActivity","Failed to set value to database: ${it.message}")
            }
    }

}



