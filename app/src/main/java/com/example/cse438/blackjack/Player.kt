package com.example.cse438.blackjack

class Player (val uid: String, var email: String, var wins: Int, var losses: Int) {

    var playerScore = 0
    var handList = ArrayList<String>()

    fun addCardToHand(c: String)
    {
        handList.add(c)
    }

    fun getTotalScore(s: Int): Int{
        playerScore += s
        return playerScore
    }

}